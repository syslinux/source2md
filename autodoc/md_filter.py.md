- \[BACK\](autodoc.md)
@file

 @brief extract MD from sources

 @author Arco van Geest <d.a.c.vangeest@tudelft.nl>


 @date 20190906 first version
 @date 20230403 Ageest doc


 Filter sourcefile with specific filter:


    'php': filter_shell,

    'cfg': filter_cfg,

    'cmd': filter_cfg,

    'Dockerfile': filter_cfg,

    'sh': filter_shell,

    'awk': filter_cfg,

    'add': filter_cfg,

    'conf': filter_cfg,

    'dox': filter_default,

    'ps1': filter_cfg,

    'pl': filter_cfg,

    'xml': filter_xml,

    'xslt': filter_xml,

    'html': filter_xml,

    'ini': filter_ini,

    'pp': filter_cfg,

    'sql': filter_sql,

    'c': filter_default,

    'h': filter_default,

    'cpp': filter_default,

    'md': filter_default,

    'py': filter_shell,

    'yaml': filter_cfg,

    'yml': filter_cfg,

    'au3': filter_cfg,

    'ipxe': filter_cfg


### filter_shell

 - lines starting with the magic '##' will be shown.
 - lines between the magics '(at)verbatim' and '(at)endverbatim' will be shown.
 - lines between the magics '## SKIP_START' and '## SKIP_END' will be skipped.


### filter_cfg

 -   lines starting with the magic '##' will be shown.
 -   lines between the magics '(at)verbatim' and '(at)endverbatim' will be shown.
 -   lines between the magics '## SKIP_START' and '## SKIP_END' will be skipped.


### filter_xml

 - lines starting with the magic '<!---' will be shown until the first '-->'.
 - lines between the magics '(at)verbatim' and '(at)endverbatim' will be shown.
 - lines between the magics '<!--- SKIP_START' and '<!--- SKIP_END' will be skipped.


### filter_ini

 -   lines starting with the magic ';;' will be shown.\n
 -   lines between the magics '(at)verbatim' and '(at)endverbatim' will be shown.\n
 -   lines between the magics ';; SKIP_START' and ';; SKIP_END' will be skipped.\n
 -   section headers [name] wil result in a section header

 section name s

### filter_sql

 -   lines starting with the magic '--!' will be shown.
 -   lines between the magics '(at)verbatim' and '(at)endverbatim' will be shown.
 -   lines between the magics '--! SKIP_START' and '--! SKIP_END' will be skipped.


### filter_default

 This is just a passthrough "filter"


## Usage

 run md_filter.py <sourcefile> to get the md output

___
