- \[BACK\](autodoc.md)
@ingroup examples
@file

 @brief Voorbeeld INI

 @version 1.0

 @author Arco van Geest <d.a.c.vangeest@tudelft.nl>

 @date 20160907 Ageest Eerste versie


 Dit is een voorbeeld ini file

@section blok1
[]blok1
]

 voorbeeld - boolean

 end
___
